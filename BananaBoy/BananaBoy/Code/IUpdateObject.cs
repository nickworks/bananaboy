﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BananaBoy.Code {
    interface IUpdateObject {
        void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState);
    }
}
