﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.Entities;
using BananaBoy.Code.Levels;
using BananaBoy.Code.Graphics;

namespace BananaBoy.Code.GameStates {
    class GSPlay : GameState {

        GUIHealth guiHealth;
        Texture2D black;
        int blackFade = 255;

        public Player player;
        Level level;
        Camera camera;

        private bool won = false;

        List<Enemy> enemies = new List<Enemy>();
        List<Powerup> powerups = new List<Powerup>();
        List<BulletFriendly> bulletFriendlies = new List<BulletFriendly>();
        List<BulletEnemy> bulletEnemies = new List<BulletEnemy>();
        List<Explosion> explosions = new List<Explosion>();

        public GSPlay(GameStateManager gsm)
            : base(gsm) {

                black = contentManager.Load<Texture2D>("black");
                guiHealth = new GUIHealth(this);

                player = new Player(this);
                camera = new Camera(player);
                level = new Level1(this, contentManager.Load<Texture2D>("block"));
        }
        public override void Draw(SpriteBatch spriteBatch) {
            DrawWithCamera(camera, spriteBatch);
            DrawGUI(spriteBatch);
        }
        public void DrawWithCamera(Camera cam, SpriteBatch spriteBatch) {

            Matrix transform = cam.transformMatrix * Game1.ScreenTransform;

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, null, null, null, null, transform);
            player.Draw(spriteBatch);

            foreach (Enemy enemy in enemies) enemy.Draw(spriteBatch);
            foreach (BulletFriendly bullet in bulletFriendlies) bullet.Draw(spriteBatch);
            foreach (BulletEnemy bullet in bulletEnemies) bullet.Draw(spriteBatch);
            foreach (Explosion explosion in explosions) explosion.Draw(spriteBatch);
            foreach (Powerup powerup in powerups) powerup.Draw(spriteBatch);

            level.Draw(spriteBatch);

            spriteBatch.End();
        }
        public void DrawGUI(SpriteBatch spriteBatch) {
            // GUI:
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            guiHealth.Draw(spriteBatch);

            if (player.dead || won) {
                blackFade += 3;
                if (blackFade > 255) blackFade = 255;
                Color color = Color.White;
                color.A = (byte)blackFade;

                spriteBatch.Draw(black, new Rectangle(0, 0, Game1.ScreenW, Game1.ScreenH), color);
                if (blackFade >= 255) {
                    if (won) gameStateManager.SwitchToGSWin();
                    else if (player.dead) gameStateManager.SwitchToGSDead();
                }
            } else {
                if (blackFade > 0) {
                    blackFade -= 3;
                    if (blackFade < 0) blackFade = 0;
                    Color color = Color.White;
                    color.A = (byte)blackFade;
                    spriteBatch.Draw(black, new Rectangle(0, 0, Game1.ScreenW, Game1.ScreenH), color);
                }
            }
            spriteBatch.End();
        }
        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            // UPDATES:
            player.Update(gameTime, keyboardState, gamePadState);
            camera.Update(gameTime, keyboardState, gamePadState);

            Enemy[] arrEnemies = enemies.ToArray();
            BulletFriendly[] arrBulletFriendlies = bulletFriendlies.ToArray();
            BulletEnemy[] arrBulletEnemies = bulletEnemies.ToArray();
            Explosion[] arrExplosions = explosions.ToArray();
            Powerup[] arrPowerups = powerups.ToArray();

            foreach (Enemy enemy in arrEnemies) {
                enemy.Update(gameTime, keyboardState, gamePadState);
                enemy.HandleCollide(player);
                if (enemy.dead) enemies.Remove(enemy);
            }
            foreach (BulletFriendly bullet in arrBulletFriendlies) {
                bullet.Update(gameTime, keyboardState, gamePadState);
                bullet.HandleCollisions(enemies);
                if (bullet.dead) bulletFriendlies.Remove(bullet);
            }
            foreach (BulletEnemy bullet in arrBulletEnemies) {
                bullet.Update(gameTime, keyboardState, gamePadState);
                bullet.HandleCollide(player);
                if (bullet.dead) bulletEnemies.Remove(bullet);
            }
            foreach (Explosion explosion in arrExplosions) {
                explosion.Update(gameTime, keyboardState, gamePadState);
                if (explosion.dead) explosions.Remove(explosion);
            }
            foreach (Powerup powerup in arrPowerups) {
                powerup.HandleCollide(player);
                if (powerup.dead) powerups.Remove(powerup);
            }
        }
        public void AddPowerup(Powerup powerup) {
            powerups.Add(powerup);
        }
        public void AddExplosion(Explosion explosion) {
            explosions.Add(explosion);
        }
        public void AddBulletFriendly(BulletFriendly bullet) {
            bulletFriendlies.Add(bullet);
        }
        public void AddBulletEnemy(BulletEnemy bullet) {
            bulletEnemies.Add(bullet);
        }
        public void AddEnemy(Enemy enemy) {
            enemies.Add(enemy);
        }
        public void Win() {
            won = true;
        }
    }
}
