﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BananaBoy.Code.GameStates {
    class GameState : IRenderObject, IUpdateObject {

        protected GameStateManager gameStateManager;
        protected ContentManager contentManager;

        public GameState(GameStateManager gsm) {
            gameStateManager = gsm;
            contentManager = gsm.NewContentManager();
        }

        public virtual void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {}
        public virtual void Draw(SpriteBatch spriteBatch) {}

        public Texture2D LoadTexture(string img) {
            return contentManager.Load<Texture2D>(img);
        }
    }
}
