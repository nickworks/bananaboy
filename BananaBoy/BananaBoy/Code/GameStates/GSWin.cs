﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.Entities;
using BananaBoy.Code.Levels;
using BananaBoy.Code.Graphics;

namespace BananaBoy.Code.GameStates {
    class GSWin : GameState {

        Texture2D black;

        SpriteText text1;
        SpriteText text2;

        public GSWin(GameStateManager gsm)
            : base(gsm) {

                black = contentManager.Load<Texture2D>("black");

                SpriteFont font = contentManager.Load<SpriteFont>("Arial");

                text1 = new SpriteText(font, new Vector2(230, 200), "YOU ARE A WINNER!");
                text2 = new SpriteText(font, new Vector2(320, 300), "Play again?");
                text1.color = Color.Green;
        }
        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            spriteBatch.Draw(black, new Rectangle(0, 0, Game1.ScreenW, Game1.ScreenH), Color.White);

            text1.Draw(spriteBatch);
            text2.Draw(spriteBatch);
            spriteBatch.End();
        }
        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {
            if (keyboardState.IsKeyDown(Keys.Enter) || gamePadState.IsButtonDown(Buttons.Start)) {
                gameStateManager.SwitchToGSPlay();
            }
        }
    }
}
