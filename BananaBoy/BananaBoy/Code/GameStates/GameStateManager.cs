﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

using BananaBoy.Code.Graphics;

namespace BananaBoy.Code.GameStates {
    class GameStateManager : IUpdateObject, IRenderObject {

        public Game1 game;

        FPSCounter spriteFPS;

        GSPlay gsPlay;
        GSTitle gsTitle;
        GSDead gsDead;
        GSWin gsWin;

        public GameStateManager(Game1 game) {
            this.game = game;

            spriteFPS = new FPSCounter(game.Content.Load<SpriteFont>("Arial"), new Vector2(5, 5));

            SwitchToGSTitle();
        }
        public void SwitchToGSPlay() {
            gsWin = null;
            gsTitle = null;
            gsDead = null;
            gsPlay = new GSPlay(this);
        }
        public void SwitchToGSTitle() {
            gsWin = null;
            gsPlay = null;
            gsDead = null;
            gsTitle = new GSTitle(this);
        }
        public void SwitchToGSDead() {
            gsWin = null;
            gsPlay = null;
            gsTitle = null;
            gsDead = new GSDead(this);
        }
        public void SwitchToGSWin() {
            gsDead = null;
            gsPlay = null;
            gsTitle = null;
            gsWin = new GSWin(this);
        }
        public void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {
            if (gsTitle != null) gsTitle.Update(gameTime, keyboardState, gamePadState);
            if (gsPlay != null) gsPlay.Update(gameTime, keyboardState, gamePadState);
            if (gsDead != null) gsDead.Update(gameTime, keyboardState, gamePadState);
            if (gsWin != null) gsWin.Update(gameTime, keyboardState, gamePadState);
            spriteFPS.Update(gameTime, keyboardState, gamePadState);
        }
        public void Draw(SpriteBatch spriteBatch) {
            if (gsTitle != null) gsTitle.Draw(spriteBatch);
            if (gsPlay != null) gsPlay.Draw(spriteBatch);
            if (gsDead != null) gsDead.Draw(spriteBatch);
            if (gsWin != null) gsWin.Draw(spriteBatch);
            spriteBatch.Begin();
            spriteFPS.Draw(spriteBatch);
            spriteBatch.End();
        }
        public ContentManager NewContentManager() {
            return new ContentManager(game.Services, "Content");
        }
    }
}
