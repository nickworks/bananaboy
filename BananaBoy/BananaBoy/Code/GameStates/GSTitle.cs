﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.Entities;
using BananaBoy.Code.Levels;
using BananaBoy.Code.Graphics;

namespace BananaBoy.Code.GameStates {
    class GSTitle : GameState {

        Texture2D black;

        SpriteText text1;
        SpriteText text2;

        SpriteIMG img1;
        SpriteIMG img2;
        SpriteText text3;

        int step = 0;
        int delay = 0;

        public GSTitle(GameStateManager gsm)
            : base(gsm) {

                black = contentManager.Load<Texture2D>("black");

                SpriteFont font = contentManager.Load<SpriteFont>("Arial");

                text1 = new SpriteText(font, new Vector2(50, 150), "In my dreams I can be whatever I want to be...");
                text2 = new SpriteText(font, new Vector2(330, 250), "so I became a banana.");
                text3 = new SpriteText(font, new Vector2(270, 400), "Press Start / Enter");

                img1 = new SpriteIMG(contentManager.Load<Texture2D>("title1"));
                img2 = new SpriteIMG(contentManager.Load<Texture2D>("title2"));

                img1.position = new Vector2(Game1.ScreenW / 2, 200);
                img2.position = img1.position;

                text1.alpha = 0;
                text2.alpha = 0;
                text3.alpha = 0;
                img1.alpha = 0;
                img2.scale = Vector2.Zero;
                
        }
        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            spriteBatch.Draw(black, new Rectangle(0, 0, Game1.ScreenW, Game1.ScreenH), Color.White);

            text1.Draw(spriteBatch);
            text2.Draw(spriteBatch);
            text3.Draw(spriteBatch);
            img1.Draw(spriteBatch);
            img2.Draw(spriteBatch);
            spriteBatch.End();
        }
        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            if (step < 9 && (keyboardState.IsKeyDown(Keys.Enter) || gamePadState.IsButtonDown(Buttons.Start))) {
                delay = 50;
                step = 9;
            }

            if (step == 0) {
                delay = 100;
                step = 1;
            } else if (step == 1) { // WAIT
                if (delay == 0) {
                    delay = 100;
                    step = 2;
                }
            } else if (step == 2) { // FADE IN text1
                FadeIn(text1);
                if (delay == 0) {
                    delay = 100;
                    step = 3;
                }
            } else if (step == 3) { // WAIT
                if (delay == 0) {
                    delay = 100;
                    step = 4;
                }
            } else if (step == 4) { // FADE IN text2
                FadeIn(text2);
                if (delay == 0) {
                    delay = 10;
                    step = 5;
                }
            } else if (step == 5) { // WAIT
                if (delay == 0) {
                    delay = 100;
                    step = 6;
                }
            } else if (step == 6) { // FADE OUT text1, text2
                FadeOut(text1);
                FadeOut(text2);
                if (delay == 0) {
                    delay = 10;
                    step = 7;
                }
            } else if (step == 7) { // WAIT
                if (delay == 0) {
                    delay = 100;
                    step = 8;
                }
            } else if (step == 8) { // FADE IN img1
                FadeIn(img1);
                if (delay == 0) {
                    delay = 50;
                    step = 9;
                }
            } else if (step == 9) { // SCALE IN img2
                text1.alpha = 0;
                text2.alpha = 0;
                img1.alpha = 255;
                ScaleIn(img2);
                if (delay == 0) {
                    step = 10;
                }
            } else if (step == 10) { // FADE IN text3
                FadeIn(text3);
                if (keyboardState.IsKeyDown(Keys.Enter) || gamePadState.IsButtonDown(Buttons.Start)) {
                    gameStateManager.SwitchToGSPlay();
                    step = 11;
                }
            }
            if (delay > 0) delay--;
        }
        private void FadeIn(Sprite s) {
            int amt = ((int)s.alpha + (int)3);
            if(amt > 255) amt = 255;
            s.alpha = (byte) amt;
        }
        private void FadeOut(Sprite s) {
            int amt = ((int)s.alpha - (int)3);
            if (amt < 0) amt = 0;
            s.alpha = (byte)amt;
        }
        private void ScaleIn(Sprite s) {
            if (s.scale.X < 1) s.scale.X += .05F;
            if (s.scale.Y < 1) s.scale.Y += .05F;
        }

    }
}
