﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BananaBoy.Code.Graphics {
    class SpriteIMG : Sprite {

        protected Texture2D texture;
        protected Rectangle crop;

        public SpriteIMG(Texture2D texture) {
            this.texture = texture;
            SetCrop(texture.Bounds);
        }
        public void SetCrop(Rectangle crop) {
            this.crop = crop;
            this.origin = new Vector2(crop.Width / 2, crop.Height / 2);   
        }
        public override void Draw(SpriteBatch spriteBatch) {
            base.Draw(spriteBatch);
            spriteBatch.Draw(texture, position, crop, color, rotation, origin, scale, spriteEffects, 1);
        }
    }
}
