﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BananaBoy.Code.Graphics {
    class Sprite : WorldObject, IRenderObject {

        public Vector2 scale = Vector2.One;
        public Vector2 origin = Vector2.Zero;
        public byte alpha = 255;

        protected float rotation = 0;
        protected SpriteEffects spriteEffects = SpriteEffects.None;
        public Color color = Color.White;

        bool squash = true;

        public virtual void Draw(SpriteBatch spriteBatch) {
            color.A = alpha;
        }
        public void AnimateFlex(float amount = .1F, float speed = .1F) {

            float large = 1 + amount;
            float small = 1 - amount;

            if (squash) {
                if (scale.X < large) {
                    scale.X += speed;
                    scale.Y -= speed;
                } else {
                    scale.Y = small;
                    squash = false;
                }
            } else {
                if (scale.X > small) {
                    scale.X -= speed;
                    scale.Y += speed;
                } else {
                    scale.Y = large;
                    squash = true;
                }
            }
        }
    }
}
