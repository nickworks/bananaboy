﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

using BananaBoy.Code;
using BananaBoy.Code.Graphics;
using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Graphics {
    class GUIHealth : IRenderObject {

        GSPlay gameState;

        SpriteIMG heart1;
        SpriteIMG heart2;
        SpriteIMG heart3;
        SpriteIMG heart4;
        SpriteIMG heart5;

        public GUIHealth(GSPlay gameState) {

            this.gameState = gameState;

            Texture2D heart = gameState.LoadTexture("heart");
            heart1 = new SpriteIMG(heart);
            heart2 = new SpriteIMG(heart);
            heart3 = new SpriteIMG(heart);
            heart4 = new SpriteIMG(heart);
            heart5 = new SpriteIMG(heart);

            heart1.position = new Vector2(Game1.ScreenW - 200, 30);
            heart2.position = new Vector2(Game1.ScreenW - 170, 30);
            heart3.position = new Vector2(Game1.ScreenW - 140, 30);
            heart4.position = new Vector2(Game1.ScreenW - 110, 30);
            heart5.position = new Vector2(Game1.ScreenW -  80, 30);
        }

        public void Draw(SpriteBatch spriteBatch) {

            
            heart1.scale = ScaleForHeart(1);
            heart2.scale = ScaleForHeart(2);
            heart3.scale = ScaleForHeart(3);
            heart4.scale = ScaleForHeart(4);
            heart5.scale = ScaleForHeart(5);

            heart1.Draw(spriteBatch);
            heart2.Draw(spriteBatch);
            heart3.Draw(spriteBatch);
            heart4.Draw(spriteBatch);
            heart5.Draw(spriteBatch);
        }
        private Vector2 ScaleForHeart(int heartNum) {

            if (heartNum < 1 || heartNum > 5) return Vector2.Zero;

            float percent = (gameState.player.health - 20 * (5 - heartNum)) / 20F;
            if (percent > 1) percent = 1;
            if (percent < 0) percent = 0;

            return new Vector2(.5f * percent);
        }
    }
}
