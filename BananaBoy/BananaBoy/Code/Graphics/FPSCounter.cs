﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace BananaBoy.Code.Graphics {
    class FPSCounter : SpriteText, IUpdateObject {

        int frameRate = 0;
        int frameCounter = 0;
        TimeSpan elapsedTime = TimeSpan.Zero;

        public FPSCounter(SpriteFont font, Vector2 position):base(font, position, "-"){

        }
        public void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {
            elapsedTime += gameTime.ElapsedGameTime;

            if (elapsedTime > TimeSpan.FromSeconds(1)) {
                elapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
            }
        }
        public override void Draw(SpriteBatch spriteBatch) {
            frameCounter++;
            text = string.Format("{0}", frameRate);
            base.Draw(spriteBatch);
        }
    }
}
