﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BananaBoy.Code.Graphics {
    /// <summary>
    /// This class renders text to the screen.
    /// </summary>
    class SpriteText : Sprite {

        /// <summary>
        /// Private member variable for the string of text.
        /// </summary>
        private string _text;
        /// <summary>
        /// Public property for the string of text. When setting the string, if it is longer (in pixels) than the maxWidth property, carriage returns will be added to wrap the text.
        /// </summary>
        /// <remarks>We should update this to intelligently only add carriage returns in whitespace.</remarks>
        public string text {
            get {
                return _text;
            }
            set {
                _text = value;
                if (maxWidth > 0) {
                    while (font.MeasureString(_text).X > maxWidth) {
                        for (int i = 0; i < _text.Length; i++) {
                            if (font.MeasureString(_text.Substring(0, i)).X > maxWidth) {
                                _text = string.Format("{0}\n{1}", _text.Substring(0, i - 2), _text.Substring(i - 2));
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Max width of the text in pixels. If less than or equal to zero, the text will not wrap.
        /// </summary>
        public int maxWidth = 0;
        /// <summary>
        /// The SpriteFont to use for the text.
        /// </summary>
        protected SpriteFont font;

        /// <summary>
        /// Constructor. Creates a new SpriteText object.
        /// </summary>
        /// <param name="font">The SpriteFont to use to render the text.</param>
        /// <param name="position">The position on-screen to render the text.</param>
        /// <param name="text">The string of text.</param>
        public SpriteText(SpriteFont font, Vector2 position, string text = "")
            : base() {

            this.font = font;
            this.position = position;
            this.text = text;
        }
        /// <summary>
        /// Overrides the superclass to draw the text to the screen.
        /// </summary>
        /// <param name="spriteBatch">Game.spriteBatch should be passed in here.</param>
        public override void Draw(SpriteBatch spriteBatch) {
            base.Draw(spriteBatch);
            spriteBatch.DrawString(font, text, position, color, rotation, origin, scale, spriteEffects, 1);
        }
    }
}
