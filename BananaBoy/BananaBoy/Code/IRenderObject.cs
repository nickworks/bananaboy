﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;

namespace BananaBoy.Code {
    interface IRenderObject {
        
        void Draw(SpriteBatch spriteBatch);
    }
}
