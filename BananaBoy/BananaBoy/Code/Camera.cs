﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BananaBoy.Code {
    class Camera : IUpdateObject {

        public Matrix transformMatrix { get; private set; }
        public Vector2 position { get; private set; }
        public WorldObject target;

        public Camera(WorldObject target = null) {
            this.target = target;
        }
        public void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {
            if (target != null) {
                float dx = target.position.X - position.X;
                float dy = target.position.Y - position.Y;

                position = target.position;
            }

            transformMatrix = Matrix.CreateTranslation(-position.X, -position.Y, 0);
        }
    }
}
