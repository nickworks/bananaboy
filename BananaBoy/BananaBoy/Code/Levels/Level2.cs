﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.Entities;
using BananaBoy.Code.Graphics;
using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Levels {
    class Level2 : Level {
        public Level2(GSPlay gameState, Texture2D texture):base(gameState, texture) {

            collisionData = new byte[,]{
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,1},
            {0,0,0,0,0,0,0,2,1,3,0,0,0,0,0,0,0,0,0,1},
            {0,0,0,0,0,0,2,1,1,1,3,0,0,0,0,0,0,0,0,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}};
            
            gameState.player.position = new Vector2(80, 80);
        }
    }
}
