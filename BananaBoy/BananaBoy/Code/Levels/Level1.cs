﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.Entities;
using BananaBoy.Code.Graphics;
using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Levels {
    class Level1 : Level {
        public Level1(GSPlay gameState, Texture2D texture):base(gameState, texture) {

            collisionData = new byte[,]{
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0},
            {1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0},
            {1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,1,1,0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0},
            {1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}};
            

            Texture2D textureBuilding = gameState.LoadTexture("building");
            Texture2D textureCandyHorzA = gameState.LoadTexture("candy-horz-A");
            Texture2D textureCandyHorzB = gameState.LoadTexture("candy-horz-B");
            Texture2D textureCandyVertA = gameState.LoadTexture("candy-vert-A");
            Texture2D textureCandyVertB = gameState.LoadTexture("candy-vert-B");
            Texture2D textureCandyCornA = gameState.LoadTexture("candy-corners-A");
            Texture2D textureCandyCornB = gameState.LoadTexture("candy-corners-B");
            Texture2D textureVine = gameState.LoadTexture("vine");
            Texture2D textureBlock = gameState.LoadTexture("block");
            Texture2D textureClothes = gameState.LoadTexture("clothes");
            Texture2D textureRadio = gameState.LoadTexture("radio");

            AddSprite(new SpriteIMG(textureBuilding), GridToWorld(new Point(9, 7)), textureBuilding.Bounds);
            AddSprite(new SpriteIMG(textureBuilding), new Vector2(576, 740), textureBuilding.Bounds);
            AddSprite(new SpriteIMG(textureBuilding), GridToWorld(new Point(0, 20)), textureBuilding.Bounds);
            AddSprite(new SpriteIMG(textureBuilding), GridToWorld(new Point(8, 22)), textureBuilding.Bounds);
            AddSprite(new SpriteIMG(textureClothes), new Vector2(256, 1428), textureClothes.Bounds);
            AddSprite(new SpriteIMG(textureRadio), GridToWorld(new Point(10, 20)), textureRadio.Bounds);

            AddSprite(new SpriteIMG(textureCandyHorzA), GridToWorld(new Point(7, 16)), new Rectangle(0,0,192,64));
            AddSprite(new SpriteIMG(textureCandyHorzA), GridToWorld(new Point(10, 16)), new Rectangle(64, 0, 128, 64));
            AddSprite(new SpriteIMG(textureCandyHorzA), GridToWorld(new Point(12, 16)), new Rectangle(64, 0, 128, 64));
            AddSprite(new SpriteIMG(textureCandyHorzA), GridToWorld(new Point(14, 16)), new Rectangle(64, 0, 192, 64));

            AddSprite(new SpriteIMG(textureCandyHorzB), GridToWorld(new Point(19, 10)), new Rectangle(0, 0, 192, 64));
            AddSprite(new SpriteIMG(textureCandyHorzB), GridToWorld(new Point(22, 10)), new Rectangle(64, 0, 192, 64));

            AddSprite(new SpriteIMG(textureVine), GridToWorld(new Point(19, 18)), textureVine.Bounds);
            AddSprite(new SpriteIMG(textureVine), GridToWorld(new Point(-1, 4)), textureVine.Bounds);

            AddSprite(new SpriteIMG(textureCandyCornA), GridToWorld(new Point(15, 21)), textureCandyCornA.Bounds);
            //AddSprite(new SpriteIMG(textureCandyCornA), GridToWorld(new Point(19, 3)), textureCandyCornA.Bounds);

            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 6)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 7)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 8)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 9)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 11)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 12)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 13)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 14)), textureBlock.Bounds);

            AddSprite(new SpriteIMG(textureCandyVertA), GridToWorld(new Point(24, 15)), textureCandyVertA.Bounds);
            AddSprite(new SpriteIMG(textureCandyVertB), GridToWorld(new Point(29, 1)), textureCandyVertA.Bounds);

            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 20)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 21)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 22)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 23)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(24, 24)), textureBlock.Bounds);

            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 6)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 7)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 8)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 9)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 10)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 11)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 12)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 13)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 14)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 15)), textureBlock.Bounds);
            //AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(29, 16)), textureBlock.Bounds);

            AddSprite(new SpriteIMG(textureBuilding), GridToWorld(new Point(29, 20)), textureBuilding.Bounds);

            AddSprite(new SpriteIMG(textureCandyCornA), GridToWorld(new Point(40, 21)), textureCandyCornA.Bounds);
            AddSprite(new SpriteIMG(textureCandyCornB), GridToWorld(new Point(42, 16)), textureCandyCornB.Bounds);

            AddSprite(new SpriteIMG(textureCandyHorzB), GridToWorld(new Point(30, 13)), new Rectangle(0, 0, 192, 64));
            AddSprite(new SpriteIMG(textureCandyHorzB), GridToWorld(new Point(33, 13)), new Rectangle(64, 0, 192, 64));

            AddSprite(new SpriteIMG(textureCandyHorzA), GridToWorld(new Point(34, 4)), new Rectangle(0, 0, 192, 64));
            AddSprite(new SpriteIMG(textureCandyHorzA), GridToWorld(new Point(37, 4)), new Rectangle(64, 0, 192, 64));

            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(35, 11)), textureBlock.Bounds);
            AddSprite(new SpriteIMG(textureBlock), GridToWorld(new Point(35, 12)), textureBlock.Bounds);

            gameState.AddEnemy(new EnemyBroccoli(gameState, new Vector2(704, 320), 60));
            gameState.AddEnemy(new EnemyBroccoli(gameState, GridToWorld(new Point(41, 20))));
            gameState.AddEnemy(new EnemyPaper(gameState, new Vector2(400, 1200)));
            gameState.AddEnemy(new EnemyPaper(gameState, new Vector2(1400, 1500)));
            gameState.AddEnemy(new EnemyCloset(gameState, GridToWorld(new Point(21, 16))));
            gameState.AddEnemy(new EnemySpikes(gameState, GridToWorld(new Point(20, 8))));
            gameState.AddEnemy(new EnemySpikesWallRight(gameState, new Vector2(480, 700)));
            gameState.AddEnemy(new EnemySpikesWallLeft(gameState, new Vector2(1683, 500)));
            gameState.AddEnemy(new EnemySpikesWallRight(gameState, new Vector2(1790, 900)));
            gameState.AddEnemy(new EnemySpikes(gameState, GridToWorld(new Point(28, 20))));
            gameState.AddEnemy(new EnemySpikes(gameState, GridToWorld(new Point(26, 20))));
            gameState.AddEnemy(new EnemyPaper(gameState, GridToWorld(new Point(32, 10))));
            gameState.AddEnemy(new EnemyCloset(gameState, GridToWorld(new Point(31, 16))));

            gameState.AddPowerup(new PowerupHeart(gameState, new Vector2(1450, 1550)));
            gameState.AddPowerup(new PowerupHeart(gameState, new Vector2(100, 250)));
            gameState.AddPowerup(new PowerupHeart(gameState, new Vector2(150, 250)));
            gameState.AddPowerup(new PowerupHeart(gameState, new Vector2(200, 250)));
            gameState.AddPowerup(new PowerupHeart(gameState, new Vector2(2620, 1550)));
            gameState.AddPowerup(new PowerupStar(gameState, new Vector2(2400, 200)));

            gameState.player.position = new Vector2(80, 500);
        }
    }
}
