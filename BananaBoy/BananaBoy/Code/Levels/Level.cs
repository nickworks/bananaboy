﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.Entities;
using BananaBoy.Code.Graphics;
using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Levels {
    class Level : IRenderObject {

        GSPlay gameState;

        static Texture2D collisionTexture;
        static bool collisionShow = false;
        static protected byte[,] collisionData;
        static Vector2 collisionSize = new Vector2(64, 64);

        List<SpriteIMG> sprites = new List<SpriteIMG>();

        public Level(GSPlay gameState, Texture2D texture) {

            this.gameState = gameState;

            collisionTexture = texture;
            collisionData = new byte[,]{
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0}};
        }
        public void Draw(SpriteBatch spriteBatch) {

            foreach (SpriteIMG sprite in sprites) {
                sprite.Draw(spriteBatch);
            }

            if (collisionShow) {
                int maxY = MaxCollisionY();
                int maxX = MaxCollisionX();
                float initialX = 0;
                float screenX = initialX;
                float screenY = 0;

                for (int y = 0; y < maxY; y++) {
                    if (screenY > Game1.ScreenH) break; // culling
                    if (screenY > -collisionSize.Y) { // culling
                        for (int x = 0; x < maxX; x++) {
                            if (screenX > Game1.ScreenW) break; // culling
                            if (screenX > -collisionSize.X) { // culling
                                if (collisionData[y, x] == 1) {
                                    Vector2 screenPosition = new Vector2(screenX, screenY);
                                    spriteBatch.Draw(collisionTexture, screenPosition, Color.White);
                                }
                            }
                            screenX += collisionSize.X;
                        }
                    }
                    screenX = initialX;
                    screenY += collisionSize.Y;
                }
            }
        }
        public void AddSprite(SpriteIMG sprite, Vector2 position, Rectangle crop) {
            sprite.SetCrop(crop);
            sprite.origin = Vector2.Zero;
            sprite.position = position;
            sprites.Add(sprite);
        }
        #region collisions
        public static int MaxCollisionY() { return (collisionData == null) ? 0 : collisionData.GetLength(0); }
        public static int MaxCollisionX() { return (collisionData == null) ? 0 : collisionData.GetLength(1); }
        public static Point WorldToGrid(Vector2 position) {
            int x = (int)Math.Floor(position.X / collisionSize.X);
            int y = (int)Math.Floor(position.Y / collisionSize.Y);
            return new Point(x, y);
        }
        public static Vector2 GridToWorld(Point position) {
            return new Vector2(GridToWorldX(position.X), GridToWorldY(position.Y));
        }
        public static float GridToWorldX(int X) {
            return X * collisionSize.X;
        }
        public static float GridToWorldY(int Y) {
            return Y * collisionSize.Y;
        }
        public static bool IsLegalXY(int x, int y) {
            return (IsLegalX(x) && IsLegalY(y));
        }
        public static bool IsLegalX(int x) {
            return (x >= 0 && x < MaxCollisionX());
        }
        public static bool IsLegalY(int y) {
            return (y >= 0 && y < MaxCollisionY());
        }
        public static int CheckCollision(Point pt) {
            return CheckCollision(pt.X, pt.Y);
        }
        public static int CheckCollision(int x, int y) {
            if (IsLegalXY(x, y)) {
                return collisionData[y, x];
            }
            return 1;
        }
        public static bool CheckAreaCollisions(Point min, Point max) {
            for (int y = min.Y; y <= max.Y; y++) {
                for (int x = min.X; x <= max.X; x++) {
                    if (CheckCollision(x, y) == 1) return true;
                }
            }
            return false;
        }
        public static void FixCollisions(PhysObj physObj) {

            if (physObj.noclip) return;

            Point cornerP1 = WorldToGrid(physObj.collisionBox.WorldCornerMin);
            Point cornerP2 = WorldToGrid(physObj.collisionBox.WorldCornerMax);

            bool collision = false;
            List<AABB> collisions = new List<AABB>();
            int num = 0;
            for (int y = cornerP1.Y; y <= cornerP2.Y; y++) {
                for (int x = cornerP1.X; x <= cornerP2.X; x++) {
                    if (CheckCollision(x, y) == 1) {
                        AABB aabb = new AABB(null, GridToWorld(new Point(x, y)), collisionSize);
                        // Check for collision boxes around this one.
                        // If this is next to another collision box,
                        // ignore checking collision on the line between the two boxes.
                        if (CheckCollision(x, y - 1) == 1) aabb.omitTop = true;
                        if (CheckCollision(x - 1, y) == 1) aabb.omitLeft = true;
                        if (CheckCollision(x + 1, y) == 1) aabb.omitRight = true;
                        if (CheckCollision(x, y + 1) == 1) aabb.omitBottom = true;
                        collisions.Add(aabb);
                        collision = true;
                        num++;
                    }
                }
            }

            if (collision == false) {
                if (physObj.grounded) { //check to see if there's ground beneath the object:
                    int y = cornerP2.Y + 1;
                    for (int x = cornerP1.X; x <= cornerP2.X; x++) {
                        if (CheckCollision(x, y) == 1) {
                            collision = true;
                            break;
                        }
                    }
                    if (!collision) physObj.GroundGone();
                }
                return;
            }

            foreach (AABB aabb in collisions) {
                Vector2 solution = aabb.SolveCollision(physObj.collisionBox.GetAABB());
                physObj.collisionBox.SetPosition(physObj.collisionBox.GetPosition() + solution);
            }
            Vector2 newpos = physObj.collisionBox.GetPosition();
            if (newpos.Y != physObj.nextPosition.Y) physObj.StopY();
            if (newpos.X != physObj.nextPosition.X) physObj.StopX();
            if (newpos.Y < physObj.nextPosition.Y) physObj.Grounded();
            if (newpos.X < physObj.nextPosition.X) physObj.GrindRight();
            if (newpos.X > physObj.nextPosition.X) physObj.GrindLeft();

            physObj.nextPosition = newpos;

        }
        #endregion
    }
}
