﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class PowerupHeart : Powerup {

        public PowerupHeart(GSPlay gameState, Vector2 position)
            : base(gameState, position, gameState.LoadTexture("heart"), new Vector2(20)) {
                scale = new Vector2(.3F);
        }

        public override Vector2 HandleCollide(PhysObj obj) {
            Vector2 overlap = base.HandleCollide(obj);
            if (overlap != Vector2.Zero) {
                if (obj.objectType == ObjectType.Player) {
                    obj.Heal(obj.healthMax/5);
                }
                Die();
            }
            return overlap;
        }
    }
}
