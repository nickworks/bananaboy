﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class BulletEnemy : PhysObj {

        public BulletEnemy(GSPlay gameState, Vector2 speed)
            : base(gameState, gameState.LoadTexture("ball"), 5) {

                this.speed = speed;
        }

        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            nextPosition = position + speed;
            Vector2 temp = nextPosition;
            ResolveCollisions();
            if (position != temp) Die();
        }
        public override Vector2 HandleCollide(PhysObj obj) {
            Vector2 overlap = base.HandleCollide(obj);
            if (overlap != Vector2.Zero) {
                if (obj.objectType == ObjectType.Player) {
                    obj.Hurt(10);
                    Die();
                }
            }
            return overlap;
        }
        protected override void Die() {
            base.Die();
        }
    }
}
