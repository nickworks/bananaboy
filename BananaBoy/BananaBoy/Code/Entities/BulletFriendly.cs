﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class BulletFriendly : PhysObj {

        public BulletFriendly(GSPlay gameState, Vector2 speed)
            : base(gameState, gameState.LoadTexture("shuriken"), 30) {

                this.speed = speed;
        }

        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            rotation += .2F;

            nextPosition = position + speed;
            Vector2 temp = nextPosition;
            ResolveCollisions();
            if (position != temp) Die();
        }

        public void HandleCollisions(List<Enemy> enemies) {
            foreach (Enemy enemy in enemies) {
                if (HandleCollide(enemy) != Vector2.Zero) {
                    enemy.Hurt(20);
                    Die();
                    break;
                }
            }
        }
        public override Vector2 HandleCollide(PhysObj obj) {
            Vector2 overlap = base.HandleCollide(obj);
            if (overlap != Vector2.Zero) {

            }
            return overlap;
        }
        protected override void Die() {
            base.Die();
        }
    }
}
