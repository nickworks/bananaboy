﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class Powerup : PhysObj {

        public Powerup(GSPlay gameState, Vector2 position, Texture2D texture, Vector2 size) :base(gameState,texture,size){

            this.position = position;
            collisionBox.SetPosition(position);
        }
    }
}