﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class PowerupStar : Powerup {

        public PowerupStar(GSPlay gameState, Vector2 position)
            : base(gameState, position, gameState.LoadTexture("star"), new Vector2(20)) {

        }

        public override Vector2 HandleCollide(PhysObj obj) {
            Vector2 overlap = base.HandleCollide(obj);
            if (overlap != Vector2.Zero) {
                if (obj.objectType == ObjectType.Player) {
                    gameState.Win();
                }
                Die();
            }
            return overlap;
        }
    }
}
