﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class EnemySpikes : Enemy {

        public EnemySpikes(GSPlay gameState, Vector2 position)
            : base(gameState, position, gameState.LoadTexture("spikes"), new Vector2(110, 64), 0) {

                collisionBox.SetOffset(new Vector2(0, 64));
                damage = 50;
                radius = 0;
        }
        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {
            
            if (!grounded) base.Update(gameTime, keyboardState, gamePadState);
        }
    }
}
