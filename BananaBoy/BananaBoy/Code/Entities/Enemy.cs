﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class Enemy : PhysObj {

        protected float radius = 200;

        protected int jumpReady = 0;
        protected int jumpjuice = 0;
        protected int jumpjuiceMax = 10;

        protected float maxWander = 200;
        protected Vector2 spawn;

        protected int damage = 10;

        public Enemy(GSPlay gameState, Vector2 position, Texture2D texture, Vector2 size, float maxWander = 200)
            : base(gameState, texture, size) {

                this.position = position;
                this.spawn = position;
                this.maxWander = maxWander;
        }
        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            if (jumpjuice > 0) {
                Push(new Vector2(0, -1));
                jumpjuice--;
            }

            base.Update(gameTime, keyboardState, gamePadState);

            if (position.X < spawn.X - maxWander) position.X = spawn.X - maxWander;
            if (position.X > spawn.X + maxWander) position.X = spawn.X + maxWander;

            if (grounded && jumpReady > 0) {
                jumpReady--;
            }
        }
        public bool IsPlayerCloseEnough() {

            Vector2 pos = gameState.player.position;
            float dx = Math.Abs(pos.X - position.X);
            float dy = Math.Abs(pos.Y - position.Y);

            if (dx < radius && dy < radius) return true;

            return false;
        }
        public void Jump() {
            if (jumpReady <= 0) {
                jumpjuice = jumpjuiceMax;
                jumpReady = 100;
            }
        }
        public bool CanGoRight() {
            return (position.X < spawn.X + maxWander);
        }
        public bool CanGoLeft() {
            return (position.X > spawn.X - maxWander) ;
        }
        public override Vector2 HandleCollide(PhysObj obj) {
            Vector2 overlap = base.HandleCollide(obj);
            if (overlap != Vector2.Zero) {
                if (obj.objectType == ObjectType.Player) {
                    obj.Hurt(damage);
                    //obj.StopX();
                    //obj.StopY();
                    Vector2 force = obj.position - position;
                    obj.Push(force);
                }
            }
            return overlap;
        }

        protected override void Die() {
            gameState.AddExplosion(new Explosion(gameState, position));
            base.Die();
        }
    }
}
