﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class EnemyPaper : Enemy {

        public EnemyPaper(GSPlay gameState, Vector2 position, float maxWander = 2000)
            : base(gameState, position, gameState.LoadTexture("enemy-paper"), new Vector2(80, 90), maxWander) {

                terminal = new Vector2(2, 20);
                radius = 500;

                healthMax = 80;
                health = healthMax;
        }
        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            if (IsPlayerCloseEnough()) {
                if (gameState.player.position.X > position.X) {
                    speed.X += 0.5F;
                } else {
                    speed.X -= 0.5F;
                }

                if (gameState.player.position.Y < position.Y - 100) {
                    Jump();
                }
            }

            AnimateFlex(.05F, .001F);
            base.Update(gameTime, keyboardState, gamePadState);
        }

    }
}
