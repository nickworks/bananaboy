﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class EnemyCloset : Enemy {

        int shootCooldown = 0;

        public EnemyCloset(GSPlay gameState, Vector2 position)
            : base(gameState, position, gameState.LoadTexture("enemy-door"), new Vector2(100, 200), 0) {

                //collisionBox.SetOffset(new Vector2(0, 20));

                radius = 600;

                healthMax = 160;
                health = healthMax;
        }
        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            if (IsPlayerCloseEnough()) {
                if (shootCooldown == 0) {

                    if (gameState.player.position.X < position.X) {
                        spriteEffects = SpriteEffects.FlipHorizontally;
                    } else {
                        spriteEffects = SpriteEffects.None;
                    }

                    float vectorSpeed = 10;
                    float dx = gameState.player.position.X - position.X;
                    float dy = gameState.player.position.Y - position.Y;
                    float d = (float) Math.Sqrt(dx * dx + dy * dy);

                    Vector2 bulletSpeed = new Vector2(vectorSpeed * dx / d, vectorSpeed * dy / d);

                    BulletEnemy bullet = new BulletEnemy(gameState, bulletSpeed);
                    bullet.position = position;
                    gameState.AddBulletEnemy(bullet);
                    shootCooldown = 30;
                } else {
                    if((new Random()).Next(0, 100) > 50) shootCooldown--;
                }
            }

            AnimateFlex(.05F, .001F);
            base.Update(gameTime, keyboardState, gamePadState);
        }

    }
}
