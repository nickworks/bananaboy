﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code;
using BananaBoy.Code.Graphics;
using BananaBoy.Code.Levels;
using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {

    public enum ExplosionType {
        EnemyDeath,
        Bubbles
    }

    class Explosion:WorldObject, IRenderObject, IUpdateObject {

        public bool dead = false;
        List<SpriteIMG> particles = new List<SpriteIMG>();
        int fadeDelay = 20;

        public Explosion(GSPlay gameState, Vector2 position, ExplosionType explosionType = ExplosionType.EnemyDeath) {

            this.position = position;

            Random random = new Random();
            int num = 20;
            if (explosionType == ExplosionType.Bubbles) num = 10;


            for (int i = 0; i < num; i++) {

                
                int vMin = 10;
                int vMax = 20;

                if (explosionType == ExplosionType.Bubbles) {
                    vMin = 5;
                    vMax = 10;
                } else {
                    vMin = 10;
                    vMax = 20;
                }

                float v = random.Next(vMin, vMax);
                float a = (float) (random.Next(0, 360) * Math.PI / 180);
                Vector2 speed = new Vector2((float)(v * Math.Cos(a)), (float)(v * Math.Sin(a)));

                float size = random.Next(50, 100) / 100F;
                Vector2 scale = new Vector2(size); 

                Texture2D texture;
                if (explosionType == ExplosionType.EnemyDeath) {

                    speed.Y -= 10;

                    switch (random.Next(1, 6)) {
                        case 1:
                            texture = gameState.LoadTexture("ball");
                            break;
                        case 2:
                            texture = gameState.LoadTexture("bubble");
                            break;
                        case 3:
                            texture = gameState.LoadTexture("fish");
                            break;
                        case 4:
                            texture = gameState.LoadTexture("heart");
                            break;
                        case 5:
                            texture = gameState.LoadTexture("shuriken");
                            break;
                        case 6:
                        default:
                            texture = gameState.LoadTexture("star");
                            break;
                    }
                } else {
                    texture = gameState.LoadTexture("bubble");
                    fadeDelay = 40;
                }

                Particle particle = new Particle(texture ,position, speed, scale);
                particle.fadeSpeed = (byte) random.Next(5, 20);
                if (explosionType == ExplosionType.Bubbles) {
                    particle.gravity = new Vector2(0, 0);
                }
                particles.Add(particle);
            }
        }
        public void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            bool done = true;
            if (fadeDelay > 0) fadeDelay--;

            foreach (Particle particle in particles) {
                particle.Update(gameTime, keyboardState, gamePadState);
                if (fadeDelay == 0) particle.shouldFade = true;
                if (!particle.done) done = false;
            }

            if (done) dead = true;
        }
        public void Draw(SpriteBatch spriteBatch) {
            foreach (Particle particle in particles) {
                particle.Draw(spriteBatch);
            }
        }
    }
}
