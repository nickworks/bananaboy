﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code;
using BananaBoy.Code.Graphics;
using BananaBoy.Code.Levels;
using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {

    public enum ObjectType {
        None,
        Player,
        Enemy,
        FriendlyBullet,
        EnemyBullet,
        Trap
    }

    class PhysObj : SpriteIMG, IUpdateObject {

        public ObjectType objectType = ObjectType.None;

        protected GSPlay gameState;

        // HEALTH STUFF
        public int health = 0;
        public int healthMax = 100;
        protected int damageTimer = 0;
        protected int damageTimerMax = 10;
        public bool dead = false;

        protected Vector2 gravity = new Vector2(0, 2);

        protected Vector2 terminal = new Vector2(10, 10);
        protected float grindSpeed = 6;
        protected Vector2 speed = Vector2.Zero;
        protected Vector2 acceleration = Vector2.Zero;
        protected Vector2 force = Vector2.Zero;

        public CollisionBox collisionBox { get; protected set; }
        public bool grounded = false;
        protected bool canWallGrind = false;
        protected bool grindWallLeft = false;
        protected bool grindWallRight = false;
        public bool noclip = false;
        public Vector2 nextPosition = Vector2.Zero;

        public PhysObj(GSPlay gameState, Texture2D texture, Vector2 size)
            : base(texture) {
                this.gameState = gameState;
                health = healthMax;
                collisionBox = new CollisionBox(this, size);
        }
        public PhysObj(GSPlay gameState, Texture2D texture, float size)
            : this(gameState, texture, new Vector2(size, size)) {
        }
        public virtual void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            if (!grounded) acceleration += gravity;
            acceleration += force;
            speed += acceleration;

            // TERMINAL VELOCITY:
            if (speed.X > terminal.X) speed.X = terminal.X;
            if (speed.X < -terminal.X) speed.X = -terminal.X;
            if (speed.Y > terminal.Y) speed.Y = terminal.Y;
            if (speed.Y < -terminal.Y) speed.Y = -terminal.Y;

            if (canWallGrind) {
                // WALL GRIND:
                if (speed.Y > grindSpeed && (grindWallLeft || grindWallRight)) speed.Y = grindSpeed;
            }
            // MOVE:
            nextPosition = position + speed;

            ResolveCollisions();
            force = Vector2.Zero;
            acceleration = Vector2.Zero;

            // DAMAGE APPEARANCE:
            if (damageTimer > 0) {
                color = Color.Red;
                damageTimer--;
            } else {
                color = Color.White;
            }
        }
        public void ResolveCollisions() {
            grindWallLeft = false;
            grindWallRight = false;
            collisionBox.SetPosition(nextPosition);
            Level.FixCollisions(this);
            position = nextPosition;
        }
        protected void SlowX() {
            if (speed.X < -.1F || speed.X > .1F) {
                speed.X *= .9F;
            } else {
                speed.X = 0;
            }
        }
        protected void SlowY() {
            if (speed.Y < -.1F || speed.Y > .1F) {
                speed.Y *= .9F;
            } else {
                speed.Y = 0;
            }
        }
        public virtual void StopX() {
            speed.X = 0;
        }
        public virtual void StopY() {
            speed.Y = 0;
        }
        public void Push(Vector2 force) {
            this.force += force;
        }
        public virtual void GrindLeft() {
            grindWallLeft = true;
        }
        public virtual void GrindRight() {
            grindWallRight = true;
        }
        public virtual void Grounded() {
            grounded = true;
            //nextPosition.Y -= .1F;
        }
        public virtual void GroundGone() {
            grounded = false;
        }
        public virtual Vector2 HandleCollide(PhysObj obj) {
            AABB aabb1 = collisionBox.GetAABB();
            AABB aabb2 = obj.collisionBox.GetAABB();

            return aabb1.SolveCollision(aabb2);
        }
        public virtual void Heal(int amount) {
            health += amount;
            if (health > healthMax) health = healthMax;
        }
        public virtual void Hurt(int amount) {
            if (damageTimer > 0) return;
            health -= amount;
            if (health <= 0) Die();
            damageTimer = damageTimerMax;
        }
        protected virtual void Die() {
            dead = true;
        }
    }
}