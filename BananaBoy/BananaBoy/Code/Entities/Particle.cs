﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code;
using BananaBoy.Code.Graphics;
using BananaBoy.Code.Levels;
using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class Particle : SpriteIMG, IUpdateObject {

        public Vector2 gravity = new Vector2(0, 1F);
        Vector2 speed;

        public bool shouldFade = false;
        public byte fadeSpeed = 20;
        //byte alpha = 255;

        public bool done = false;

        public Particle(Texture2D texture, Vector2 position, Vector2 speed, Vector2 scale)
            : base(texture) {

                this.position = position;
                this.speed = speed;
                this.scale = scale;
        }
        public void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {
            speed += gravity;
            position += speed;

            speed *= .9F;

            if (shouldFade && alpha > 0) {
                if (alpha >= fadeSpeed) alpha -= fadeSpeed;
                else alpha = 0;
            }

            color = Color.White;
            color.A = alpha;

            if (alpha <= 0) done = true;
        }
    }
}
