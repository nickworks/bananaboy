﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.Graphics;
using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class Player: PhysObj {

        bool facingRight = true;

        int shootCooldown = 0;

        // JUMP STUFF
        int jumpjuice = 0;
        int jumpjuiceMax = 10;
        bool jumpButtonDown = false;
        bool jumpDouble = false;
        Vector2 jumpVector = Vector2.Zero;

        // ANIMATION STUFF
        float rotationSpeed = 0;

        public Player(GSPlay gameState)
            : base(gameState, gameState.LoadTexture("banana"), 80) {

                objectType = ObjectType.Player;
                terminal = new Vector2(8, 20);
                SetCrop(new Rectangle(0, 0, 80, 80));
                spriteEffects = SpriteEffects.FlipHorizontally;
                canWallGrind = true;
        }
        public override void Draw(SpriteBatch spriteBatch) {
            if(!dead) base.Draw(spriteBatch);
        }
        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            if (dead) return;

            if (damageTimer > 0) {
                SetCrop(new Rectangle(80, 0, 80, 80));
            } else {
                SetCrop(new Rectangle(0, 0, 80, 80));
            }

            float thumbX = gamePadState.ThumbSticks.Left.X;
            float thumbY = gamePadState.ThumbSticks.Left.Y;

            bool keyUp = keyboardState.IsKeyDown(Keys.Up) || thumbY > .5F;
            bool keyDown = keyboardState.IsKeyDown(Keys.Down) || thumbY < -.5F;
            bool keyLeft = keyboardState.IsKeyDown(Keys.Left) || thumbX < -.5F;
            bool keyRight = keyboardState.IsKeyDown(Keys.Right) || thumbX > .5F;
            bool keyX = keyboardState.IsKeyDown(Keys.X) || gamePadState.IsButtonDown(Buttons.A);
            bool keyZ = keyboardState.IsKeyDown(Keys.Z) || gamePadState.IsButtonDown(Buttons.X);
            bool keyShift = keyboardState.IsKeyDown(Keys.LeftShift) || gamePadState.IsButtonDown(Buttons.LeftShoulder);

            if (keyLeft) {
                facingRight = false;
            } else if (keyRight) {
                facingRight = true;
            }

            if (keyLeft && !keyShift) {
                speed.X -= 1F;
            } else if (keyRight && !keyShift) {
                speed.X += 1F;
            } else {
                SlowX();
            }
            // SHOOT:
            if (keyZ) {
                if (shootCooldown == 0) {
                    Vector2 bulletSpeed = Vector2.Zero;

                    if (grindWallLeft) {
                        if (keyUp) {
                            bulletSpeed = new Vector2(0, -15);
                        } else if (keyDown && !grounded) {
                            bulletSpeed = new Vector2(0, 15);
                        } else {
                            bulletSpeed = new Vector2(15, 0);
                        }
                    } else if (grindWallRight) {
                        if (keyUp) {
                            bulletSpeed = new Vector2(0, -15);
                        } else if (keyDown && !grounded) {
                            bulletSpeed = new Vector2(0, 15);
                        } else {
                            bulletSpeed = new Vector2(-15, 0);
                        }
                    } else if (keyRight) {
                        if (keyUp) {
                            bulletSpeed = new Vector2(12, -9);
                        } else if (keyDown && !grounded) {
                            bulletSpeed = new Vector2(12, 9);
                        } else {
                            bulletSpeed = new Vector2(15, 0);
                        }
                    } else if (keyLeft) {
                        if (keyUp) {
                            bulletSpeed = new Vector2(-12, -9);
                        } else if (keyDown && !grounded) {
                            bulletSpeed = new Vector2(-12, 9);
                        } else {
                            bulletSpeed = new Vector2(-15, 0);
                        }
                    } else if (keyUp) {
                        bulletSpeed = new Vector2(0, -15);
                    } else if (keyDown && !grounded) {
                        bulletSpeed = new Vector2(0, 15);
                    } else {
                        bulletSpeed = new Vector2(15, 0);
                        if (!facingRight) bulletSpeed *= -1;
                    }

                    BulletFriendly bullet = new BulletFriendly(gameState, bulletSpeed);
                    bullet.position = position;
                    gameState.AddBulletFriendly(bullet);
                    shootCooldown = 5;
                } else {
                    shootCooldown--;
                }
            } else {
                shootCooldown = 0;
            }

            // JUMP:
            if (keyX) {
                Jump();
                jumpButtonDown = true;
            } else {
                jumpjuice = 0;
                jumpButtonDown = false;
            }

            AnimateFlex(.1F, .01F);
            
            base.Update(gameTime, keyboardState, gamePadState);

            //if (speed.X > 2) spriteEffects = SpriteEffects.FlipHorizontally;
            //if (speed.X < -2) spriteEffects = SpriteEffects.None;

            // ROLL:
            if (grounded) {
                rotationSpeed = speed.X * .02F;
            }
            rotation += rotationSpeed;

        }
        private void Jump() {

            if (!jumpButtonDown) {
                if (grounded) {
                    speed.Y = 0;
                    jumpVector = new Vector2(0, -200F);
                    jumpjuice = jumpjuiceMax;
                    jumpDouble = false;
                } else if (grindWallLeft) {
                    speed.Y = 0;
                    jumpVector = new Vector2(100F, -170F);
                    jumpjuice = jumpjuiceMax;
                    jumpDouble = false;
                } else if (grindWallRight) {
                    speed.Y = 0;
                    jumpVector = new Vector2(-100F, -170F);
                    jumpjuice = jumpjuiceMax;
                    jumpDouble = false;
                } else if (!jumpDouble) {
                    speed.Y = 0;
                    jumpVector = new Vector2(0, -200F);
                    jumpjuice = jumpjuiceMax;
                    jumpDouble = true;

                    gameState.AddExplosion(new Explosion(gameState, position, ExplosionType.Bubbles));

                }
            }

            if (jumpjuice > 0) {
                if (jumpjuice % 3 == 0) {
                    //SetAnimation("jump");
                    Push(jumpVector);
                    grounded = false;
                }
                jumpjuice--;
            }
        }
        public override void StopY() {
            base.StopY();
            if (!grounded) jumpjuice = 0;
        }
        public override void GrindLeft() {
            base.GrindLeft();
            //spriteEffects = SpriteEffects.FlipHorizontally;
            //rotation = 0;
            rotationSpeed *= .9F;
        }
        public override void GrindRight() {
            base.GrindRight();
            //spriteEffects = SpriteEffects.None;
            //rotation = 0;
            rotationSpeed *= .9F;
        }
        public override void GroundGone() {
            base.GroundGone();
            jumpjuice = 0;
        }
        protected override void Die() {
            base.Die();
            gameState.AddExplosion(new Explosion(gameState, position));
        }
    }
}
