﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BananaBoy.Code.GameStates;

namespace BananaBoy.Code.Entities {
    class EnemyBroccoli : Enemy {

        public EnemyBroccoli(GSPlay gameState, Vector2 position, float maxWander = 400)
            : base(gameState, position, gameState.LoadTexture("enemy-broccoli"), new Vector2(100, 100), maxWander) {

                collisionBox.SetOffset(new Vector2(0, 20));

                terminal = new Vector2(5, 20);
                radius = 500;

                health = 120;
                health = healthMax;

                damage = 20;

        }
        public override void Update(GameTime gameTime, KeyboardState keyboardState, GamePadState gamePadState) {

            if (IsPlayerCloseEnough()) {
                if (gameState.player.position.X > position.X) {
                    speed.X += 1F;
                } else {
                    speed.X -= 1F;
                }

                if (gameState.player.position.Y < position.Y - 100) {
                    Jump();
                }
            }

            AnimateFlex(.05F, .001F);
            base.Update(gameTime, keyboardState, gamePadState);
        }

    }
}
