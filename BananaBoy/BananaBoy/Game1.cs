using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using BananaBoy.Code.GameStates;

namespace BananaBoy {
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        static private Game1 game;
        static public int ScreenW { get; private set; }
        static public int ScreenH { get; private set; }
        static public Matrix ScreenTransform;
        static public void SetResolution(int W, int H) {
            ScreenW = W;
            ScreenH = H;
            ScreenTransform = Matrix.CreateTranslation(W / 2, H / 2, 0);
            game.graphics.PreferredBackBufferWidth = W;
            game.graphics.PreferredBackBufferHeight = H;
            game.graphics.ApplyChanges();
        }


        GameStateManager gameStateManager;

        public Game1() {
            game = this;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        protected override void Initialize() {
            base.Initialize();

            SetResolution(800, 500);
            gameStateManager = new GameStateManager(this);
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }
        protected override void UnloadContent() {
            // TODO: Unload any non ContentManager content here
        }
        protected override void Update(GameTime gameTime) {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            gameStateManager.Update(gameTime, Keyboard.GetState(), GamePad.GetState(PlayerIndex.One));

            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            gameStateManager.Draw(spriteBatch);            

            base.Draw(gameTime);
        }
        public Texture2D LoadTexture(string str) {
            return Content.Load<Texture2D>(str);
        }
        
    }
}
